/*
 
*  GSMIgnition
 
*/
 
//Include ArduPi library
 
#include "arduPi.h"
 
int resetModulePin = 9;
 
int onModulePin = 8;        
 
void switchModuleOn(){
 
   digitalWrite(onModulePin,HIGH);
 
   delay(2000);
 
   digitalWrite(onModulePin,LOW);
 
}
 
void resetModule(){
 
   digitalWrite(resetModulePin,HIGH);
 
   delay(500);
 
   digitalWrite(resetModulePin,LOW);
 
   delay(100);
 
}
 
int main (){
 
   Serial.begin(115200);                
 
   delay(2000);
 
   pinMode(resetModulePin, OUTPUT);
 
   pinMode(onModulePin, OUTPUT);
 
   Serial.flush();
 
   printf ("zero\n");
 
   Serial.print("AT");    
 
   delay(1000);
 
   if (Serial.available()==0)
 
      {
 
      printf ("uno\n");
 
      resetModule();
 
      delay(2000);
 
      }
 
   Serial.print("AT");
 
   delay(1000);
 
   if (Serial.available()==0)
 
      {
 
      printf ("due\n");
 
      switchModuleOn();                 
 
      }
 
return (0);
 
}