/*  
 *  GPRS+GPS Quadband Module (SIM908)
 *  
 *  Copyright (C) Libelium Comunicaciones Distribuidas S.L. 
 *  http://www.libelium.com 
 *  
 *  This program is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or 
 *  (at your option) any later version. 
 *  
 *  This program is distributed in the hope that it will be useful, 
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License 
 *  along with this program.  If not, see http://www.gnu.org/licenses/. 
 *  
 *  Version:           2.0.1
 *  Design:            David Gascón 
 *  Implementation:    Alejandro Gallego & Marcos Martinez
 *  Modification:      Dezhi Fu
 *
 *  This program gets GPS-data from the module every second and sends the data to
 *  the dedicated server, which is specified in the variables section.
 */

 //Include arduPi library
#
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <limits>
#include <cstdio>
#include "arduPi.h"

using namespace std;

 // function declaration
 int8_t sendATcommand2(const char* ATcommand, const char* expected_answer1, const char* expected_answer2, unsigned int timeout);
 int8_t sendATcommand(const char* ATcommand, const char* expected_answer, unsigned int timeout);
 void power_on();
 int8_t start_GPS();
 int8_t reset_GPS();
 int8_t get_GPS();
 int8_t writeGPS2file();
 fstream& GotoLine(fstream& file, unsigned int num);
 int8_t buildTcp();
 int8_t sendData();
 int8_t recordCounter(const char *filename, int recordData);
 int readCounter(const char *filename);
 bool fexists(const char *filename);
 


  // variable declaration
int8_t answer;
int onModulePin= 2;
char aux_str[100];
char confirm[]="\x1A";
int counter;
int lineWritten;
int lineRead;
long previous;
char gpsRecordHistory[] = "lineWrittenFile.txt";
char gpsSentHistory[] = "lineReadFile.txt";

  // put mobile service provider information here
char pin[]="*******";
char apn[]="internet.eplus.de";
char user_name[]="eplus";
char password[]="gprs";
char IP_address[]="gpslog.ddns.net";
  // tcp server port
char port[]="2345";

  // NMEA sentence
char Basic_str[100];

char sendString[2048]="Test string";

  // prerequisite steps

void setup(){
    // Is there already data saved on the machine?
    if (fexists(gpsRecordHistory)==false){
        // if not then initiate counter and save it to file
        lineWritten = 1;
        recordCounter(gpsRecordHistory, lineWritten);
    }
    else{
        lineWritten = readCounter(gpsRecordHistory);
        cout << "Lines written: " << lineWritten << endl;
    }
   

    // Is there already data saved and not sent to server?
    if (fexists(gpsSentHistory)==false){
        // if not then initiate counter and save it to file
        lineRead = 1;
        recordCounter(gpsSentHistory, lineRead);
    }
    else{
        lineRead = readCounter(gpsSentHistory);
        cout << "Lines sent: " <<lineRead << endl;
    }

    // Delete position file when 100 positions are logged and sent to save SD card memory
    if(lineRead==100 && lineRead==lineWritten){
        const int result = remove("./gpslog.txt");
        lineRead = 1;
        lineWritten = 1;
    }
    pinMode(onModulePin, OUTPUT);
    Serial.begin(115200);    
 
    printf("Starting...\n");
    //power_on();
    cout << "Connect to server..." << endl;
    buildTcp();

    start_GPS();
    // enter PIN of SIM card if needed
    //snprintf(aux_str, sizeof(aux_str), "AT+CPIN=%s", pin);
    sendATcommand2("AT+CPIN?", "OK", "ERROR", 2000);
    
    
    printf("Connecting to the cellular network...\n");
    // register to network, CREG results are: 
    //   0 - unsolicited result code network registration disabled
    //   1 - registered, home network
    //   5 - registered, roaming
    while( sendATcommand2("AT+CREG?", "+CREG: 0,1", "+CREG: 0,5", 1000)== 0 );
}


void loop(){
    // Check if GPS is fixed
    cout << "Checking GPS Status... " << endl;
    if(sendATcommand("AT+CGPSSTATUS?", "2D Fix", 2000) || 
        sendATcommand("AT+CGPSSTATUS?", "3D Fix", 2000) == 1)
    {
        // gps fixed
    }
    //start GPS using warm start
    else
    {
        cout << "Reset GPS using warm start..." << endl;
        reset_GPS();
    }

    get_GPS();
    writeGPS2file();
    sendData();
}

void power_on(){

    uint8_t answer=0;
    
    // checks if the module is started
    answer = sendATcommand("AT", "OK", 2000);
    if (answer == 0)
    {
        // power on pulse
        digitalWrite(onModulePin,HIGH);
        delay(3000);
        digitalWrite(onModulePin,LOW);
    
        // waits for an answer from the module
        while(answer == 0){     // Send AT every two seconds and wait for the answer
            answer = sendATcommand("AT", "OK", 2000);    
        }
    }
    
}

bool fexists(const char *filename) {
  ifstream ifile(filename);
  return ifile;
}

int8_t sendATcommand(const char* ATcommand, const char* expected_answer, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '\0', 100);    // Initialize the string

    delay(100);

    while( Serial.available() > 0) Serial.read();    // Clean the input buffer

    Serial.println(ATcommand);    // Send the AT command 


        x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        if(Serial.available() != 0){    
            // if there are data in the UART input buffer, reads it and checks for the asnwer
            response[x] = Serial.read();
            printf("%c",response[x]);
            x++;
            // check if the desired answer  is in the response of the module
            if (strstr(response, expected_answer) != NULL)    
            {
				printf("\n");
                answer = 1;
            }
        }
    }
    // Waits for the asnwer with time out
    while((answer == 0) && ((millis() - previous) < timeout));    

        return answer;
}

int8_t sendATcommand2(const char* ATcommand, const char* expected_answer1, 
        const char* expected_answer2, unsigned int timeout){

    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '\0', 100);    // Initialize the string

    delay(100);

    while( Serial.available() > 0) Serial.read();    // Clean the input buffer

    Serial.println(ATcommand);    // Send the AT command 

    x = 0;
    previous = millis();

    // this loop waits for the answer
    do{
        // if there are data in the UART input buffer, reads it and checks for the asnwer
        if(Serial.available() != 0){    
            response[x] = Serial.read();
            printf("%c",response[x]);
            x++;
            // check if the desired answer 1  is in the response of the module
            if (strstr(response, expected_answer1) != NULL)    
            {
				printf("\n");
                answer = 1;
            }
            // check if the desired answer 2 is in the response of the module
            else if (strstr(response, expected_answer2) != NULL)    
            {
				printf("\n");
                answer = 2;
            }
        }
    }
    // Waits for the answer with time out
    while((answer == 0) && ((millis() - previous) < timeout));    

    return answer;
}

int8_t start_GPS(){

    unsigned long previous;
    unsigned long timeout = 120000;

    previous = millis();
    // closes the GPS
    sendATcommand("AT+CGPSPWR=0", "OK", 2000);
    // starts the GPS
    sendATcommand("AT+CGPSPWR=1", "OK", 2000);
    cout << "Search for GPS satellites using cold start..." << endl;
    // cold start
    sendATcommand("AT+CGPSRST=0", "OK", 2000);

    // waits for fix GPS
    while( (sendATcommand("AT+CGPSSTATUS?", "2D Fix", 5000) || 
        sendATcommand("AT+CGPSSTATUS?", "3D Fix", 5000)) == 0 );

    if ((millis() - previous) < timeout)
    {
        printf("Time to fix GPS: %u ms\n",(millis() - previous));
        return 1;
    }
    else
    {
        printf("%u minutes timeout reached.",timeout/1000*60);
        printf("Time to fix GPS: %u ms\n",(millis() - previous));
        return 0;    
    }
}

int8_t reset_GPS(){

    unsigned long previous;
    unsigned long timeout = 120000;
    previous = millis();
 // closes the GPS
    sendATcommand("AT+CGPSPWR=0", "OK", 2000);
    // starts the GPS
    sendATcommand("AT+CGPSPWR=1", "OK", 2000);
    // warm start
    sendATcommand("AT+CGPSRST=1", "OK", 2000);

    // waits for fix GPS
    while( (sendATcommand("AT+CGPSSTATUS?", "2D Fix", 5000) || 
        sendATcommand("AT+CGPSSTATUS?", "3D Fix", 5000)) == 0 );

    if ((millis() - previous) < timeout)
    {
        printf("Time to fix GPS: %u ms\n",(millis() - previous));
        return 1;
    }
    else
    {
        printf("%u minutes timeout reached.\n",timeout/1000*60);
        printf("Time to fix GPS: %u ms\n",(millis() - previous));
        return 0;    
    }
}


int8_t recordCounter(const char *filename, int recordData){
    ofstream fout;
    fout.open(filename);
    fout << recordData;
    fout.close();
    return 0;
}

int readCounter(const char *filename){
    int counter;
    fstream file(filename);
    GotoLine(file, 1);
    string str;
    file >> str;
    stringstream convert(str);
    if ( !(convert >> counter) ) {
        counter=0;
    }
    return counter;
}

int8_t writeGPS2file(){

    ofstream outfile;
    outfile.open("gpslog.txt", std::ios_base::app);
    outfile << Basic_str << "\n";
    cout << "Position written to line " << lineWritten  << " in datafile." << endl;
    lineWritten++;
    recordCounter(gpsRecordHistory, lineWritten);
    
   
    return 0;

}

int8_t sendData(){
    memset(sendString, '\0', 2048);    // Initialize the string
    fstream file("gpslog.txt");
    stringstream ss;
    int i = lineRead;
    while (i < lineRead + 20)
    {
        GotoLine(file, i);
        string str;
        file >> str; 
        ss<<str<<";";
        if (i==lineWritten){break;}
        cout << "Position on line " << i << " in datafile read." << endl;
        i++;
    }
    string s = ss.str();
    strncpy(sendString, s.c_str(),sizeof(sendString));
    cout << "Length of to be sent data: " << s.size() << endl;
    // send data according to length of String variables 
    sprintf(aux_str,"AT+CIPSEND");
    cout << '\n' << "Sending data..." << endl;
    // Sends GPS data to the TCP socket
    if (sendATcommand2(aux_str, ">", "ERROR", 5000)==2){ 
        cout << "Sending data failed. Save the position data until connection is restored." << endl;
        cout << "Restoring TCP connection...";
        buildTcp(); 
    }
    else{
    answer=sendATcommand2(sendString, "SEND OK", "ERROR", 2000);
    cout <<"Confirm sending using CTRL+Z..." << endl;
    sendATcommand(confirm,"ERROR",2000);

    lineRead=i;
    recordCounter(gpsSentHistory, lineRead);




    return 0;
 
  }


}

int8_t buildTcp(){
    long previous;
    previous = millis();
    int delayFactor = 1;
    while((millis() - previous) < 10000){
 
// Selects Single-connection mode
    if (sendATcommand2("AT+CIPMUX=0", "OK", "ERROR", 1000/delayFactor) == 1)
    {
        // Waits for status IP INITIAL
        while(sendATcommand("AT+CIPSTATUS", "INITIAL", 500/delayFactor)  == 0 );
        //delay(500);
        // set the AT-command for using GPRS Connection
        snprintf(aux_str, sizeof(aux_str), "AT+CSTT=\"%s\",\"%s\",\"%s\"", apn, user_name, password);
        // Sets the APN, user name and password
        if (sendATcommand2(aux_str, "OK",  "ERROR", 30000/delayFactor) == 1)
        {            
            // Waits for status IP START
            while(sendATcommand("AT+CIPSTATUS", "START", 500/delayFactor)  == 0 );
            //delay(500);
            
            // Brings Up Wireless Connection
            if (sendATcommand2("AT+CIICR", "OK", "ERROR", 30000/delayFactor) == 1)
            {
                // Waits for status IP GPRSACT
                while(sendATcommand("AT+CIPSTATUS", "GPRSACT", 500/delayFactor)  == 0 );
                //delay(500);
                
                // Gets Local IP Address
                if (sendATcommand2("AT+CIFSR", ".", "ERROR", 1000/delayFactor) == 1)
                {
                    // Waits for status IP STATUS
                    while(sendATcommand2("AT+CIPSTATUS", "IP STATUS", "", 5000/delayFactor)  == 0 );
                    //delay(500);
                    printf("Opening TCP\n");
                    
                    snprintf(aux_str, sizeof(aux_str), "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"", IP_address, port);
                    

                    if (sendATcommand2(aux_str, "CONNECT OK", "CONNECT FAIL", 30000/delayFactor) == 1)
                    {
                        printf("Connected\n");

                        break;
  
                    }
                    else
                    {
                        printf("Error opening the connection\n");
                        // Closes the socket
                        sendATcommand2("AT+CIPCLOSE", "CLOSE OK", "ERROR", 1000/delayFactor);
                    }  
                }
                else
                {
                    printf("Error getting the IP address\n");
                }  
            }
            else
            {
                printf("Error bring up wireless connection\n");
            }
        }
        else
        {
            printf("Error setting the APN\n");
        } 
    }
    else
    {
        printf("Error setting the single connection\n");
        sendATcommand2("AT+CIPSHUT", "OK", "ERROR", 10000/delayFactor);

    }
  }
}
std::fstream& GotoLine(std::fstream& file, unsigned int num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return file;
}

int8_t get_GPS(){

    int8_t counter, answer;
    long previous;

    
    // First get the NMEA string
    // Clean the input buffer
    while( Serial.available() > 0) Serial.read(); 
    // request Basic string
    cout << "Requesting GPS position..." << endl;
    sendATcommand("AT+CGPSINF=0", "AT+CGPSINF=0\r\n\r\n", 1000);
    cout << "GPS Position retrieved." << endl;
    counter = 0;
    answer = 0;
    memset(Basic_str, '\0', 100);    // Initialize the string
    previous = millis();
    // this loop waits for the NMEA string
    do{

        if(Serial.available() != 0){    
            Basic_str[counter] = Serial.read();
            counter++;
            // check if the desired answer is in the response of the module
            if (strstr(Basic_str, "OK") != NULL)    
            {
                answer = 1;
            }
        }
        // Waits for the asnwer with time out
    }
    while((answer == 0) && ((millis() - previous) < 1000));  

    Basic_str[counter-3] = '\0'; 
    
    return answer;
}


int main (){
   
    
    setup();
    while(1){
        loop();
    }
    return (0);
}

    