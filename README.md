# README #


### What is this repository for? ###

- A collection of programs to collect vehicle data coming from sensors, postprocess and transmit it to a dedicated server
- Developed for the Raspberry Pi (Raspberry Pi 2 Model B was used)
- Captured data is saved on SD disk using txt-files
- This was developed in conjunction with a Java proxy program, which can be found on https://bitbucket.org/jd_fu/telemetryserver.git
- Version 0.1

### How do I get set up? ###

- You need the arduPi library from libelium. A version, which was used for the development, is included to this repository
- In order to execute the programs after booting the system init-system files are included
- No tests are written so far

### Contribution guidelines ###

- I welcome every advice and contribution

### Who do I talk to? ###

- You can e-mail me on jacky.fu@mytum.de